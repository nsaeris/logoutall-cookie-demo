/**
 *
 */
/**
 *
 */

var Hapi = require('hapi');

var server = new Hapi.Server();
server.connection({ host: 'app1.aeriscloud.com', port: 3000 }, function() {});

var currentUser = "";
var cookieName = "email";
    server.route({
        method: 'POST',
        path: '/testpost',

        handler: function(request, reply) {
          return reply("OK");
        },
    });

    server.route({
        method: 'GET',
        path: '/login',
        handler: function(request, reply) {
          var email = request.query.email;
          currentUser = email;
          reply.state(cookieName, email, {domain: '.aeriscloud.com'});
        	return reply(email);
        }
    });

    server.route({
        method: 'GET',
        path: '/resource',
        handler: function(request, reply) {
          var cookie = request.state[cookieName];
          if (cookie === currentUser) {
            return reply('Same User ' + currentUser);
          } else {
            console.log('Different User [cookie = ' + cookie + ', currentUser = ' + currentUser);
            return reply.redirect("/logout");
          }
        }
    });

    server.route({
        method: 'GET',
        path: '/logout',
        handler: function(request, reply) {
        	reply.unstate(cookieName);
          currentUser = "";
        	return reply("Logout " + currentUser);
        }
    });



    server.start(function () {
        console.log('Server running at:', server.info.uri);
    });
