# README #

# Setup
* git clone
* npm install hapi
* node app1.js //This will start app on http://app1.aeriscloud.com:3000
* node app2.js //This will start app on http://app2.aeriscloud.com:3030
* edit /etc/hosts to add entry for app1.aeriscloud.com and app2.aeriscloud.com. Both will point to 127.0.0.1


# Test
* Login to app1 as user1@aeris.net

http://app1.aeriscloud.com:3000/login?email=user1@aeris.net

* Login to app2 as user1@aeris.net in another tab. This simulate user navigating to another app with SSO

http://app2.aeriscloud.com:3030/login?email=user1@aeris.net

* Access app1 and app2 resource. Both will show current user as user1@aeris.net

http://app1.aeriscloud.com:3000/resource
http://app2.aeriscloud.com:3030/resource

* Logout from app2 and login as user2@aeris.net

http://app2.aeriscloud.com:3030/logout
http://app2.aeriscloud.com:3030/login?email=user2@aeris.net

* Access app1 resource. User should get logged out.

http://app1.aeriscloud.com:3000/resource